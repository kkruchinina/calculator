package ru.kkk.calculator.interfaces;

public interface IConstants {
    String[] signs = { "+", "-", "/", "*", "^"};
    String[] trig = {"sin(", "cos(", "tg(", "asin(",
            "acos(", "atg("};
    String ERROR = "Ошибка";
}
