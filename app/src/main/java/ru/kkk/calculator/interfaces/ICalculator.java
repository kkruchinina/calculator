package ru.kkk.calculator.interfaces;

public interface ICalculator {
    Double startCalculate(String expression);
}
